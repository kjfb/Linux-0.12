###  tty 等待队列中缓冲区操作宏

源码：</br>
```c
#define TTY_BUF_SIZE 1024				// tty 缓冲区（缓冲队列）大小【必须为2的次幂】

// tty 字符缓冲队列数据结构
struct tty_queue {
	unsigned long head;					// 缓冲区中数据头指针
	unsigned long tail;					// 缓冲区中数据尾指针
	char buf[TTY_BUF_SIZE];				// 队列的缓冲区
};

// 以下定义了 tty 等待队列中缓冲区操作宏

// 缓冲区指针 a 前移 1 字节，若已超出缓冲区右侧，则指针循环。（TTY_BUF_SIZE必须为2的次幂）
#define INC(a) ((a) = ((a)+1) & (TTY_BUF_SIZE-1))

// a 后退 1 字节，并循环
#define DEC(a) ((a) = ((a)-1) & (TTY_BUF_SIZE-1))

// 清空指定队列的缓冲区
#define EMPTY(a) ((a)->head == (a)->tail)

// 缓冲区还能存放字符的长度（空闲区长度）
#define LEFT(a) (((a)->tail-(a)->head-1)&(TTY_BUF_SIZE-1))

// 缓冲区中最后一个位置
#define LAST(a) ((a)->buf[(TTY_BUF_SIZE-1)&((a)->head-1)])

//缓冲区满（若为 1 的话）
#define FULL(a) (!LEFT(a))

//缓冲区中已存放字符的长度（字符数）
#define CHARS(a) (((a)->head-(a)->tail)&(TTY_BUF_SIZE-1))

// 从 queue 队列项缓冲区中 tail 处取一字符，并且让 tail+=1
#define GETCH(queue,c) \
(void)({c=(queue)->buf[(queue)->tail];INC((queue)->tail);})

// 往 queue 队列项缓冲区中 head 处放置一字符，并且让 head+=1
#define PUTCH(c,queue) \
(void)({(queue)->buf[(queue)->head]=(c);INC((queue)->head);})
```