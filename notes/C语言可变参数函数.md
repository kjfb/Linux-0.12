## C语言可变参数函数

###  什么是可变参数函数
* 在C语言编程中有时会遇到一些参数可变的函数，例如printf()、scanf()，其函数原型为：
    ```c
    int printf(const char * format, ...)
    int scanf(const char *format, ...)
    ```
* 就拿 printf 来说吧，它除了有一个参数 format 固定以外，后面的参数其个数和类型都是可变的，用三个点“...”作为参数占位符。

### 参数列表的构成
* 任何一个可变参数的函数都可以分为两部分：固定参数和可选参数。至少要有一个固定参数，其声明与普通函数参数声明相同；可选参数由于数目不定(0个或以上)，声明时用"..."表示。固定参数和可选参数共同构成可变参数函数的参数列表。

### 头文件：#include <stdarg.h>
* 该头文件包含一个类型(va_list)和三个宏(va_start, va_arg 和 va_end)

### 使用步骤：
1. #include <stdarg.h> // 包含头文件
2. va_list ap; // 定义一个 va_list 类型的变量
3. va_start(ap, v); // 初始化 ap 指针，使其指向第一个可变参数。v 是变参列表的前一个参数
4. va_arg(ap, type); // 返回当前变参值,并使 ap 指向列表中的下个变参, type是程序员需要显性的告诉编译器当前变量的类型
5. va_end(ap); // 将指针 ap 置为无效，结束变参的获取

### 注意事项
* 变参宏无法智能识别可变参数的数目和类型，因此实现变参函数时需自行判断可变参数的数目和类型，这个需要自己想办法，比如显式提供参数个数或设定遍历结束条件，主调函数和被调函数约定好变参的数目和类型等
* va_arg(ap, type)宏中的 type 不可指定为以下类型：char short float

### 示例
* 代码：
    ```c
    #include <stdio.h>
    #include <stdarg.h>

    void test_func(int arg1, ...)
    {
        va_list va;
        va_start(va, arg1);
        printf("arg1 = %d\n", arg1);
        printf("arg2 = %d\n", va_arg(va, int));
        printf("arg3 = %d\n", va_arg(va, int));
        printf("arg4 = %d\n", va_arg(va, int));
        va_end(va);
    }

    int main(void)
    {
        test_func(1, 2, 3, 4);
        return 0;
    }
    ```
* 运行结果：
    ```c
    arg1 = 1
    arg2 = 2
    arg3 = 3
    arg4 = 4
    ```

