### C语言版Lambda表达式

* 原型： (void)({ 执行语句 });

源码：</br>
```c
#include <stdio.h>

#define Lambda(i, j) (void)({(i)++; (j)++;})

int main(void)
{
    int i = 0, j = 0;
    // (void)({i++; j++;});
    Lambda(i, j);
    printf("i = %d, j = %d\n", i, j);
    return 0;
}
```