## 从开机到main函数的执行分三步完成

### 第一步：启动BIOS
* 计算机在上电的一瞬间，计算机的内存（RAM）中空空如也，什么程序都没有。而系统程序是存储在启动介质中（早期操作系统是存储在软盘中，现在启动介质多种多样，如硬盘、flash、mmc等等），但是CPU在设计的时候只能运行内存中的程序，没有能力直接从启动介质中运行操作系统。那么这个时候就需要BIOS将启动介质中的程序搬运到内存（RAM）中去。
* BIOS启动原理：从上面的叙述可以得出结论，BIOS本身是不可能被任何软件方式启动的，那么BIOS的启动就只能靠硬件了。
* BIOS程序被固化在计算机主板上的一块很小的ROM芯片里，通常不同的主板所用的BIOS也有所不同。就启动部分而言，各种类型的BIOS的基本原理大致相似

### 第二步：从启动盘加载操作系统到内存(boot)
* bootsect
    * 第一步（复制bootsect）：BIOS完成一系列自检等工作后会将启动盘第一个扇区（512字节，这个扇区就是linux的引导程序，由bootsect.S汇编而成）复制到到内存0x7c00处,程序跳转到0x7c00处开始执行bootsect。bootsect首先要作的工作就是规划内存，接下来，bootsect启动程序将它自身（全部512B内容）复制到内存绝对地址0x90000开始处继续执行
    * 第二步（将setup程序加载到内存中）：bootsect在内存中的位置是0x90000-0x90200,setup被复制到紧挨着booscct的位置
    * 第三步（将内核代码加载到内存中）：内核代码被加载的位置：0x10000
* setup
    * 通过BIOS提取机器系统数据，机器系统数据所占的空间为0x90000~0x91FD,共510字节，即原来的bootsect只有2个字节未被覆盖，bootsect刚使用结束，setup执行时立即将其用数据覆盖，内存使用效率极高，当然，这也与当时的硬件条件不无关系。当一些准备工作完成之后，setup关掉所有的中断，将位于0x10000的内核程序复制到内存0x00000处，0x00000原存放BIOS建立的中断向量表及BIOS数据区，此时BIOS16位中断被废除，需要setup重新构建32位中断机制，中断向量表起始位置仍在0x00000处。
* head
    * head程序与之前bootsect与setup加载方式有所不同，head.s先汇编成目标代码，将用C语言编写的内核程序编译成目标带代码，然后链接system模块。也就是说system模块中既包含了内核程序，又包含了head程序，head程序在前。setup将system模块复制到内存0x00000位置，实际上head程序就在0x00000这个位置。head程序除了为main做好准备工作之外，还给内存空间创建了内存分页机制
    
### 第三步：为执行32位的main函数做过渡工作
* 关中断并将system移动到内存起始位置0x00000
* 设置中断描述符表和全局描述符表
* 开启32位寻址，最大寻址空间4G
* 为保护模式下执行head.s作准备
* head.s开始执行
