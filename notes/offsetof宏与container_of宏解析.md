### offsetof宏与container_of宏解析

```c
#define offsetof(type, member) ((unsigned int) &((type *)0)->member)
```
* 功能：计算某成员在类型中偏移位置的宏
* (type *)0 - 将0地址强制转换为type * 指针
* ((type *)0)->member - 指向成员member
* &((type *)0)->member - 取成员member的地址
* (unsigned int) &((type *)0)->member - 将成员member的地址强转成unsigned int类型即为成员member在类型中偏移位置（减0省略）
```c
#define container_of(ptr, type, member) ({			\
    const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
    (type *)( (char *)__mptr - offsetof(type,member) );})
```
* 功能：通过已知的一个数据结构成员指针ptr，数据结构类型type，以及这个成员指针在这个数据结构中的成员名member，来获取指向这个数据结构的指针type *
* 首先，将0地址强制转换为type * 指针
* 使用typeof()获取名为member成员的数据类型
* 定义一个临时成员变量指针__ptr，让它指向已知的成员变量指针ptr。
* 获取这个成员变量在这个数据结构的偏移量。
* 现在已经知道成员member的地址以及在数据结构的偏移量，将__mptr强制转换为char * 再减去偏移量就是要获取数据结构的地址。
