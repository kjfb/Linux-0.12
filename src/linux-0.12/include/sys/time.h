#ifndef _SYS_TIME_H
#define _SYS_TIME_H

/* gettimofday returns this */		// gettimeofday()函数返回该时间结构		
struct timeval {
	long	tv_sec;					// 秒
	long	tv_usec;				// 微秒
};

// 时间区结构。tz 为时区（Time Zone）的缩写，DST（Daylight Saving Time）是夏令时的缩写
struct timezone {
	int	tz_minuteswest;				// 格林威治西部分钟时间
	int	tz_dsttime;					// 夏令时区调整时间
};

#define	DST_NONE	0				/* not on dst */ // 非夏令时
#define	DST_USA		1				/* USA style dst */ // USA 形式的夏令时
#define	DST_AUST	2				/* Australian style dst */ // 澳洲形式的夏令时
#define	DST_WET		3				/* Western European dst */ // 西欧形式的夏令时
#define	DST_MET		4				/* Middle European dst */ // 中欧形式的夏令时
#define	DST_EET		5				/* Eastern European dst */ // 东欧形式的夏令时
#define	DST_CAN		6				/* Canada */ // 加拿大
#define	DST_GB		7				/* Great Britain and Eire */ // 英国和爱尔兰
#define	DST_RUM		8				/* Rumania */ // 罗马尼亚
#define	DST_TUR		9				/* Turkey */ // 土耳其
#define	DST_AUSTALT	10				/* Australian style with shift in 1986 */

// 文件描述符集的设置宏，用于 select()函数
#define FD_SET(fd,fdsetp)	(*(fdsetp) |= (1 << (fd)))		// 在集中设置 fd 描述符	
#define FD_CLR(fd,fdsetp)	(*(fdsetp) &= ~(1 << (fd)))		// 从集中删除 fd 描述符
#define FD_ISSET(fd,fdsetp)	((*(fdsetp) >> fd) & 1)			// 判断 fd 是否在集中
#define FD_ZERO(fdsetp)		(*(fdsetp) = 0)					// 清空描述符集

/*
 * Operations on timevals.
 *
 * NB: timercmp does not work for >= or <=.
 */
/*
 * 用于 timevals 上的操作。
 * 注意：这里的 timercmp 不能用于 >= 或 <=。
 */
// timeval 时间结构的操作函数
#define	timerisset(tvp)		((tvp)->tv_sec || (tvp)->tv_usec)
#define	timercmp(tvp, uvp, cmp)	\
	((tvp)->tv_sec cmp (uvp)->tv_sec || \
	 (tvp)->tv_sec == (uvp)->tv_sec && (tvp)->tv_usec cmp (uvp)->tv_usec)
#define	timerclear(tvp)		((tvp)->tv_sec = (tvp)->tv_usec = 0)

/*
 * Names of the interval timers, and structure
 * defining a timer setting.
 */
/* 内部定时器名称和结构，用于定义定时器设置。 */
#define	ITIMER_REAL		0			// 以实际时间递减
#define	ITIMER_VIRTUAL	1			// 以进程虚拟时间递减
#define	ITIMER_PROF		2			// 以进程虚拟时间或者当系统运行时以进程时间递减

// 内部时间结构。其中 it（Internal Timer）是内部定时器的缩写
struct	itimerval {
	struct	timeval it_interval;	/* timer interval */
	struct	timeval it_value;		/* current value */
};

#include <time.h>					// 时间类型头文件。其中最主要定义了 tm 结构和一些有关时间的函数原形
#include <sys/types.h>				// 类型头文件。定义了基本的系统数据类型

int gettimeofday(struct timeval * tp, struct timezone * tz);
int select(int width, fd_set * readfds, fd_set * writefds,
	fd_set * exceptfds, struct timeval * timeout);

#endif /*_SYS_TIME_H*/
